package user;
import view.LoginView;

import javax.swing.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class Client {
    private static String SERVER_ADDRESS = "jopnigga";
    private static final int SERVER_PORT = 8080;

    public static void main(String[] args) {
        try {
            // Try to connect to the server
            Socket socket = new Socket(SERVER_ADDRESS, SERVER_PORT);
            InetAddress serverAddress = InetAddress.getByName(SERVER_ADDRESS);
            System.out.println("Connected to server. " + serverAddress);
            ObjectInputStream reader = new ObjectInputStream(socket.getInputStream());

            // If connection is successful, open the LoginView
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    new LoginView(socket).setVisible(true);
                }
            });
            while(true){
                try {
                    Object response = reader.readObject();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            // If connection fails, show an error message
            JOptionPane.showMessageDialog(null, "Failed to connect to the server.", "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }
}
