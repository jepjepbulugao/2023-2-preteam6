package server;

import model.LoginModel;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

  public class Server {
   public static void main(String[] args) {
      try{
         // Creates a new server socket
         ServerSocket serverSocket = new ServerSocket(8080);
         String inetAddress = InetAddress.getLocalHost().getCanonicalHostName();
         System.out.println("Server started. Listening on " + inetAddress + " port 8080....");
         Socket clientSocket = serverSocket.accept();
         ObjectOutputStream writer = new ObjectOutputStream(clientSocket.getOutputStream());
         ObjectInputStream reader = new ObjectInputStream(clientSocket.getInputStream());
         System.out.println("Client connected: " + clientSocket);
         handleClient(reader, writer);
      } catch (Exception e) {
         e.printStackTrace();
      }
   }
      private static void handleClient (ObjectInputStream reader, ObjectOutputStream writer){
         // Handle client communication in a separate thread
         ClientHandler clientHandler = new ClientHandler(reader, writer);
         Thread thread = new Thread(clientHandler);
         thread.start();
      }
   }


class ClientHandler implements Runnable {
   private ObjectOutputStream writer;
   private  ObjectInputStream reader;

   public ClientHandler(ObjectInputStream reader, ObjectOutputStream writer) {
      this.reader = reader;
      this.writer = writer;

   }

   @Override
   public void run() {
      try{
         String email = reader.readUTF();
         String password = reader.readUTF();
         boolean admin = reader.readBoolean();
         System.out.println("email: " + email);
         System.out.println("password: " + password);
         System.out.println(admin);
      }catch (Exception e) {
         e.printStackTrace();
      }

   }
}


