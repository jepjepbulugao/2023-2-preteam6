package view;

import javax.swing.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;

import java.awt.*;
import java.io.File;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

public class CreateEventView extends JFrame {
    private JTextField nameField;
    private JTextField dateField;
    private JTextField timeField;
    private JTextField venueField;
    private JTextField descriptionField;
    private boolean eventCreatedSuccessfully;

    public CreateEventView(ClientView parentPanel) {
        setTitle("Create Event");
        setSize(300, 200);

        JPanel myPanel = new JPanel();
        myPanel.setLayout(new GridLayout(5, 2));
        myPanel.add(new JLabel("Name:"));
        nameField = new JTextField(10);
        myPanel.add(nameField);
        myPanel.add(new JLabel("Date:"));
        dateField = new JTextField(10);
        myPanel.add(dateField);
        myPanel.add(new JLabel("Time:"));
        timeField = new JTextField(10);
        myPanel.add(timeField);
        myPanel.add(new JLabel("Venue:"));
        venueField = new JTextField(10);
        myPanel.add(venueField);
        myPanel.add(new JLabel("Description:"));
        descriptionField = new JTextField(10);
        myPanel.add(descriptionField);

        int result = JOptionPane.showConfirmDialog(null, myPanel,
                "Please Enter Event Details", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            try {
                File inputFile = new File("_2023-2-preteam6/res/Events.xml");
                if (!inputFile.exists()) {
                    JOptionPane.showMessageDialog(null, "Events XML file not found.");
                    return;
                }

                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(inputFile);
                doc.getDocumentElement().normalize();

                Element root = doc.getDocumentElement();

                Element newEvent = doc.createElement("event");

                Element name = doc.createElement("name");
                name.appendChild(doc.createTextNode(nameField.getText()));
                newEvent.appendChild(name);

                Element date = doc.createElement("date");
                date.appendChild(doc.createTextNode(dateField.getText()));
                newEvent.appendChild(date);

                Element time = doc.createElement("time");
                time.appendChild(doc.createTextNode(timeField.getText()));
                newEvent.appendChild(time);

                Element venue = doc.createElement("venue");
                venue.appendChild(doc.createTextNode(venueField.getText()));
                newEvent.appendChild(venue);

                Element description = doc.createElement("description");
                description.appendChild(doc.createTextNode(descriptionField.getText()));
                newEvent.appendChild(description);

                root.appendChild(newEvent);

                // Write the updated XML file
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                DOMSource source = new DOMSource(doc);
                StreamResult resultFile = new StreamResult(new File("_2023-2-preteam6/res/Events.xml"));
                transformer.transform(source, resultFile);

                // Set eventCreatedSuccessfully to true
                eventCreatedSuccessfully = true;
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Error occurred while creating event: " + e.getMessage(),
                        "Error", JOptionPane.ERROR_MESSAGE);
                // Set eventCreatedSuccessfully to false
                eventCreatedSuccessfully = false;
            }
        } else {
            // If user cancels the operation, set eventCreatedSuccessfully to false
            eventCreatedSuccessfully = false;
        }

        // Close the CreateEventView window and bring mainPanel to front
        setVisible(false);
        parentPanel.setVisible(true);

        // Display message dialog if event was created successfully
        if (eventCreatedSuccessfully) {
            JOptionPane.showMessageDialog(parentPanel, "Event created successfully!");
        }
    }

    // Getter method for eventCreatedSuccessfully
    public boolean isEventCreatedSuccessfully() {
        return eventCreatedSuccessfully;
    }
}
