package view;

import javax.swing.*;
import java.awt.*;

public class ClientView extends JFrame {
    private DefaultListModel<String> eventListModel;
    private JList<String> eventList;
    private JButton createButton;
    private JButton editButton;
    private JButton deleteButton;
    private JButton searchButton;
    private JButton refreshButton; // New refresh button
    private JTextField searchField;
    private JTextArea eventContentArea;

    public ClientView() {
        setTitle("Event Manager");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        getContentPane().setBackground(new Color(240, 240, 240));

        eventListModel = new DefaultListModel<>();
        eventList = new JList<>(eventListModel);
        JScrollPane scrollPane = new JScrollPane(eventList);

        createButton = new JButton("Create Event");
        editButton = new JButton("Edit Event");
        deleteButton = new JButton("Delete Event");
        searchButton = new JButton("Search");
        refreshButton = new JButton("Refresh"); // Initialize refresh button

        Font buttonFont = new Font("Segoe UI", Font.PLAIN, 20);

        createButton.setFont(buttonFont);
        editButton.setFont(buttonFont);
        deleteButton.setFont(buttonFont);
        searchButton.setFont(buttonFont);
        refreshButton.setFont(buttonFont); // Set font for refresh button

        JPanel buttonPanel = new JPanel(new GridLayout(3, 1, 0, 20));
        buttonPanel.setBackground(new Color(240, 240, 240));
        buttonPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        buttonPanel.add(createButton);
        buttonPanel.add(editButton);
        buttonPanel.add(deleteButton);

        JPanel searchPanel = new JPanel(new BorderLayout());
        searchPanel.setBackground(new Color(240, 240, 240));
        searchPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        searchField = new JTextField();
        searchField.setFont(new Font("Segoe UI", Font.PLAIN, 20));
        searchButton.setFont(new Font("Segoe UI", Font.PLAIN, 20));
        refreshButton.setFont(new Font("Segoe UI", Font.PLAIN, 20)); // Set font for refresh button
        JPanel searchButtonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT)); // Create a panel to hold search and refresh buttons
        searchButtonPanel.setBackground(new Color(240, 240, 240));
        searchButtonPanel.add(searchButton);
        searchButtonPanel.add(refreshButton); // Add refresh button to the right of search button
        searchPanel.add(searchField, BorderLayout.CENTER);
        searchPanel.add(searchButtonPanel, BorderLayout.EAST);

        eventContentArea = new JTextArea();
        eventContentArea.setEditable(false);
        eventContentArea.setFont(new Font("Segoe UI", Font.PLAIN, 20));
        eventContentArea.setBorder(BorderFactory.createEmptyBorder(20, 20, 0, 20));
        JScrollPane eventContentScrollPane = new JScrollPane(eventContentArea);

        createButton.addActionListener(e -> {
            CreateEventView dialog = new CreateEventView(this);
        });
        editButton.addActionListener(e -> editEvent());
        deleteButton.addActionListener(e -> deleteEvent());
        searchButton.addActionListener(e -> searchEvents(searchField.getText()));
        searchField.addActionListener(e -> searchEvents(searchField.getText()));
        refreshButton.addActionListener(e -> refreshEventList()); // Add action listener for refresh button

        setLayout(new BorderLayout(20, 20));
        add(searchPanel, BorderLayout.NORTH);
        add(buttonPanel, BorderLayout.WEST);
        add(scrollPane, BorderLayout.CENTER);
        add(eventContentScrollPane, BorderLayout.SOUTH);

        // Add an empty panel to create space on the right side
        JPanel emptyPanel = new JPanel();
        emptyPanel.setBackground(new Color(240, 240, 240)); // Match background color
        add(emptyPanel, BorderLayout.EAST);

        pack();
        setLocationRelativeTo(null);

        // Move the list to the left by setting its preferred width
        scrollPane.setPreferredSize(new Dimension(200, scrollPane.getPreferredSize().height));
    }

    private void refreshEventList() {
        // Logic to refresh event list, you can reload events from XML file or database
    }

    private void editEvent() {
        String selectedEvent = eventList.getSelectedValue();
        if (selectedEvent != null) {
            refreshEventList();
        } else {
            JOptionPane.showMessageDialog(this, "Please select an event to edit.");
        }
    }

    private void deleteEvent() {
        int selectedIndex = eventList.getSelectedIndex();
        if (selectedIndex != -1) {
            // Implement deletion logic
            refreshEventList();
        } else {
            JOptionPane.showMessageDialog(this, "Please select an event to delete.");
        }
    }

    private void searchEvents(String searchText) {
        if (searchText.isEmpty()) {
            // If the search text is empty, reset the event list to show all events
            refreshEventList();
        } else {
            DefaultListModel<String> filteredListModel = new DefaultListModel<>();
            for (int i = 0; i < eventListModel.getSize(); i++) {
                String eventName = eventListModel.getElementAt(i);
                // Case-insensitive search by checking if the event name contains the search text
                if (eventName.toLowerCase().contains(searchText.toLowerCase())) {
                    filteredListModel.addElement(eventName);
                }
            }
            eventList.setModel(filteredListModel); // Set the filtered list model to display the search results
        }
    }


    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        SwingUtilities.invokeLater(() -> {
            ClientView eventManager = new ClientView();
            eventManager.setVisible(true);
        });
    }
}
