import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class AdminView extends JFrame {
    private JList<String> userList;
    private DefaultListModel<String> userListModel;
    private JTextField searchField;
    private JButton searchButton;
    private JButton addButton;
    private JButton editButton;
    private JButton deleteButton;

    public AdminView() {
        setTitle("Manage Users");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        getContentPane().setBackground(new Color(240, 240, 240));

        // Initialize components
        initializeComponents();

        // Set layout
        setLayout(new BorderLayout(20, 20));
        add(createSearchPanel(), BorderLayout.NORTH);
        add(createButtonPanel(), BorderLayout.WEST);
        add(createUserListPanel(), BorderLayout.CENTER);

        pack();
        setLocationRelativeTo(null);

        // Load users from XML file
        loadUsersFromXML("_2023-2-preteam6/res/Accounts.xml");
    }

    private void initializeComponents() {
        userListModel = new DefaultListModel<>();
        userList = new JList<>(userListModel);
        searchField = new JTextField();
        searchButton = new JButton("Search");
        addButton = new JButton("Add User");
        editButton = new JButton("Edit User");
        deleteButton = new JButton("Delete User");

        Font buttonFont = new Font("Segoe UI", Font.PLAIN, 20);
        addButton.setFont(buttonFont);
        editButton.setFont(buttonFont);
        deleteButton.setFont(buttonFont);

        searchButton.setFont(new Font("Segoe UI", Font.PLAIN, 20));
        searchField.setFont(new Font("Segoe UI", Font.PLAIN, 20));
    }

    private JPanel createSearchPanel() {
        JPanel searchPanel = new JPanel(new BorderLayout());
        searchPanel.setBackground(new Color(240, 240, 240));
        searchPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        searchPanel.add(searchField, BorderLayout.CENTER);
        searchPanel.add(searchButton, BorderLayout.EAST);
        return searchPanel;
    }

    private JPanel createButtonPanel() {
        JPanel buttonPanel = new JPanel(new GridLayout(3, 1, 0, 20));
        buttonPanel.setBackground(new Color(240, 240, 240));
        buttonPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        buttonPanel.add(addButton);
        buttonPanel.add(editButton);
        buttonPanel.add(deleteButton);
        return buttonPanel;
    }

    private JScrollPane createUserListPanel() {
        JPanel userListPanel = new JPanel(new BorderLayout());
        userListPanel.setBackground(new Color(240, 240, 240));
        userListPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        userListPanel.add(new JScrollPane(userList), BorderLayout.CENTER); // Adding JList to JScrollPane

        JPanel emptyPanel = new JPanel(); // Create an empty panel to center the JList horizontally
        emptyPanel.setBackground(new Color(240, 240, 240));
        userListPanel.add(emptyPanel, BorderLayout.EAST); // Add the empty panel to the right side

        return new JScrollPane(userListPanel);
    }

    private void loadUsersFromXML(String filename) {
        try {
            File xmlFile = new File(filename);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            NodeList accountList = doc.getElementsByTagName("account");
            System.out.println("Number of accounts: " + accountList.getLength());
            for (int i = 0; i < accountList.getLength(); i++) {
                Node accountNode = accountList.item(i);
                if (accountNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element accountElement = (Element) accountNode;
                    NodeList usernameList = accountElement.getElementsByTagName("username");
                    if (usernameList.getLength() > 0) {
                        String username = usernameList.item(0).getTextContent();
                        System.out.println("Username: " + username);
                        userListModel.addElement(username);
                    } else {
                        System.out.println("No username found for account element: " + i);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Error loading users from XML file: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        SwingUtilities.invokeLater(() -> {
            AdminView manageUsersView = new AdminView();
            manageUsersView.setVisible(true);
        });
    }
}
