package model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class UserModel {

    private String accountType;
    private String username;
    private static String email;
    private static String password;

    private String fileInput = "_2023-2-preteam6/res/Accounts.xml";

    public void getEmail(String email) {
        this.email = email;
    }


    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAccountType() {
        return accountType;
    }

    public String getUsername() {
        return username;
    }

    public static String getEmail() {
        return email;
    }

    public static String getPassword() {
        return password;
    }

    /**
     * readUserAccount:
     * A method to read the account from the XML file.
     * @param fileInput
     */
    public static void readUserAccounts(String fileInput) {
        try (BufferedReader inputFile = new BufferedReader(new FileReader((fileInput)))) {
            inputFile.read(); //subject for reviewing
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean login(String email, String password) {
        // Check if the provided email and password match with the user's credentials
        return this.email.equals(email) && this.password.equals(password);
    }
}
