package model;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;

public class EventModel {
    private static String eventName;
    private String eventDate;
    private String eventTime;
    private String eventVenue;

    /**
     * Default Constructor
     */
    public EventModel() {
        this.eventName = null;
        this.eventDate = null;
        this.eventTime = null;
        this.eventVenue = null;
    }

    /**
     * Constructor with parameters
     * @param name
     * @param date
     * @param time
     * @param venue
     */
    public EventModel(String name, String date, String time, String venue) {
        this.eventName = name;
        this.eventDate = date;
        this.eventTime = time;
        this.eventVenue = venue;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    public void setEventVenue (String eventVenue) {
        this.eventVenue = eventVenue;
    }

    public static String getEventName() {
        return eventName;
    }

    public String getEventDate() {
        return eventDate;
    }

    public String getEventTime() {
        return eventTime;
    }

    public String getEventVenue() {
        return eventVenue;
    }

    // Additional methods

    /**
     * readEventsFromXML():
     * A method to read the events from the XML file.
     * @param fileInput
     */
    public void readEventsFromXML(String fileInput) {
        try (BufferedReader inputFile = new BufferedReader(new FileReader(fileInput))) {
            String line;
            while(inputFile.readLine() != null) {
                // storing the event
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void createEvent (){
        try {
            File eventsFile = new File("_2023-2-preteam6/res/Events.xml");
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();

            Element event = doc.createElement("event");
            doc.appendChild(event);

            Element date = doc.createElement("date");
            date.setTextContent("2024-02-06");
            event.appendChild(date);

            Element time = doc.createElement("time");
            time.setTextContent("6:00 PM");
            event.appendChild(time);

            Element venue = doc.createElement("venue");
            venue.setTextContent("Cafe");
            event.appendChild(venue);

            Element description = doc.createElement("description");
            description.setTextContent("22nd");
            event.appendChild(description);


            // Write the DOM document to a file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("_2023-2-preteam6/res/Events.xml"));
            transformer.transform(source, result);


        } catch (ParserConfigurationException | TransformerException e) {
            e.printStackTrace();
        }
    }

    public void searchEvent (){

    }

}


