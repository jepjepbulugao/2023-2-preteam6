package model;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class RequestModel {
    private String accountType;
    private String email;
    private String username;
    private String password;

    private String fileInput = "_2023-2-preteam6/res/Accounts.xml";

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static ArrayList<String> readUserAccounts(String fileInput) {
        List<String> usernames = new ArrayList<>();

        try (BufferedReader inputFile = new BufferedReader(new FileReader(fileInput))) {
            String line;
            StringBuilder xmlContent = new StringBuilder();
            while ((line = inputFile.readLine()) != null) {
                xmlContent.append(line.trim());
            }

            // Parse XML content
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new InputSource(new StringReader(xmlContent.toString())));

            NodeList accountNodes = document.getElementsByTagName("account");
            for (int i = 0; i < accountNodes.getLength(); i++) {
                Node accountNode = accountNodes.item(i);
                if (accountNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element accountElement = (Element) accountNode;
                    String username = accountElement.getElementsByTagName("username").item(0).getTextContent();
                    usernames.add(username);
                }
            }
        } catch (IOException | ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }

        return (ArrayList<String>) usernames;
    }

}
