package model;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class LoginModel{
    private String userEmail;

    private String password;
    public LoginModel(String userEmail, String password){
        this.userEmail = userEmail;
        this.password = password;
    }
    public List<String> login(String inputEmail, String inputPassword) {
        List<String> details = new ArrayList<>();
        try {
            File accountsFile = new File("res/accounts.xml");
            DocumentBuilderFactory docBuildFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuildFactory.newDocumentBuilder();
            Document document = docBuilder.parse(accountsFile);
            document.getDocumentElement().normalize();
            NodeList adminNodes = document.getElementsByTagName("admin");
            NodeList userNodes = document.getElementsByTagName("user");
            for (int i = 0; i < userNodes.getLength(); i++) {
                Node node = userNodes.item(i);
                Element element = (Element) node;
                String userType = element.getAttribute("type");
                String userEmail = element.getElementsByTagName("email")
                        .item(0)
                        .getTextContent();
                String password = element.getElementsByTagName("password")
                        .item(0)
                        .getTextContent();
                if (inputEmail.equalsIgnoreCase(userEmail)) {
                    boolean correctPassword;
                    do {
                        String decryptedPassword = inputPassword;
                        if (decryptedPassword.contentEquals(password)) {
                            String errPassword = "Wrong";
                            details.add(errPassword);
                            correctPassword = false;
                        } else {
                            details.add(userType);
                            correctPassword = true;
                        }
                    } while (!correctPassword);

                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return details;
    }
}

