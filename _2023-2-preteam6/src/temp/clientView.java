package temp;

import javax.swing.*;
import java.awt.*;

public class clientView extends JFrame {
    private JPanel panel;
    private JTextField searchField;
    private JButton checkButtonLeft, deleteButton, joinButton, editButton, checkButtonMiddle, retrieveButton;
    private JList leftColumn, middleColumn, rightColumn;

    public clientView() {
        panel = new JPanel();
        panel.setLayout(new GridLayout(1, 3));

        // Left column
        checkButtonLeft = new JButton("Check");
        deleteButton = new JButton("Delete");
        joinButton = new JButton("Join");
        editButton = new JButton("Edit");
        JPanel leftPanel = new JPanel();
        leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
        leftPanel.add(checkButtonLeft);
        leftPanel.add(deleteButton);
        leftPanel.add(joinButton);
        leftPanel.add(editButton);
        leftColumn = new JList();
        leftPanel.add(new JScrollPane(leftColumn));
        panel.add(leftPanel);

        // Middle column
        checkButtonMiddle = new JButton("Check");
        JPanel middlePanel = new JPanel();
        middlePanel.setLayout(new BoxLayout(middlePanel, BoxLayout.Y_AXIS));
        middlePanel.add(checkButtonMiddle);
        middleColumn = new JList();
        middlePanel.add(new JScrollPane(middleColumn));
        panel.add(middlePanel);

        // Right column
        retrieveButton = new JButton("Retrieve");
        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
        rightPanel.add(retrieveButton);
        rightColumn = new JList();
        rightPanel.add(new JScrollPane(rightColumn));
        panel.add(rightPanel);

        // Search field
        searchField = new JTextField();
        getContentPane().add(searchField, BorderLayout.NORTH);

        getContentPane().add(panel, BorderLayout.CENTER);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new clientView());
    }
}
