package temp;

public class EventView {
    private String eventName;
    private String date;
    private String time;
    private String venue;
    private String description;

    public EventView(String eventName, String date, String time, String venue, String description) {
        this.eventName = eventName;
        this.date = date;
        this.time = time;
        this.venue = venue;
        this.description = description;
    }

    // Getters and setters for the fields
    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
