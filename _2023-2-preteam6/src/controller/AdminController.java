package controller;

import model.RequestModel;

import javax.swing.*;

import java.util.ArrayList;


public class AdminController {

    private DefaultListModel<String> userModel; // userModel defined as a class-level variable

    public AdminController() {
    }

    public void processRequest() {
        // Instantiate userModel if not already done
        if (userModel == null) {
            userModel = new DefaultListModel<>();
        }

        // Read user accounts
        ArrayList<String> usernames = RequestModel.readUserAccounts("_2023-2-preteam6/res/Requests.xml");

        // Display user accounts in GUI
        //AdminView.displayUserAccounts(usernames, userModel);
    }

    // Method to accept a user account request
    public void acceptRequest(String username) {
        // Example of accepting a user account request
        System.out.println("Accepted account request for: " + username);
        // You can implement the logic to process the accepted request here
    }

    // Method to reject a user account request
    public void rejectRequest(String username) {
        // Example of rejecting a user account request
        System.out.println("Rejected account request for: " + username);
        // You can implement the logic to process the rejected request here
    }

    public static void main(String[] args) {
        // Create an instance of AdminController
        AdminController adminController = new AdminController();

        // Call the processRequest method to start processing requests
        adminController.processRequest();
    }
}